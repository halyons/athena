#include "InDetRegionSelector/InDetRegionSelectorLUT.h"
#include "InDetRegionSelector/SiRegionSelectorTable.h"
#include "InDetRegionSelector/FTK_RegionSelectorTable.h"
#include "InDetRegionSelector/TRT_RegionSelectorTable.h"

DECLARE_COMPONENT( InDetDD::InDetRegionSelectorLUT )
DECLARE_COMPONENT( SiRegionSelectorTable )
DECLARE_COMPONENT( TRT_RegionSelectorTable )
DECLARE_COMPONENT( FTK_RegionSelectorTable )

